const services = require("../services");

//this middleware function binds the services to the request objects
function serviceMiddleware() {
  return function (req, res, next) {
    req.services = services;
    next();
  };
}
module.exports = serviceMiddleware;
