const session = require('express-session');
const MongoStore = require('connect-mongo')(session); //store session on MongoDb
const mongoose = require('mongoose');

module.exports = {
  secret: 'wibgewe13f13', //random string
  resave: false,
  saveUninitialized: false,
  proxy: true,
  cookie: {
    secure: process.env.NODE_ENV && process.env.NODE_ENV.indexOf('development') > -1 ? false : true,
  }, //secure needs to be set to true for production here
  expires: new Date(new Date()),
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
};
